﻿#
# Package Created June 30, 2016
# Packaged by Austin Wall
# Last Updated ...

Import-Module -Name "S:\Powershell\InstallMonkeyTest.psm1" -Force

New-Package -Name ImageJ -Revision 20160630T1508 -Version 1_50

$source = $package.SourceFiles
$applogs = Get-Applogs
$appid = $package.ID
$path = "C:\Program Files(x86)\ImageJ"
$startmenu = Get-AllUsersStartMenu


function install{
    Write-Logs "Moving Files from $source to $path"
    if(!Test-Path $path){
        New-Item $path -ItemType "directory"
    }
    $success = Copy-Item "$source\*" $path -recurse
}

function postinstall{
    $shortcutdir = $startmenu+"\ImageJ"
    Write-Logs "Creating Shortcut in $shortcutdir"
    New-Shortcut -target "$path\imagej.1_50\ImageJ.exe" -link "$shortcutdir\IamgeJ.lnk" -description "Launches ImageJ"

}

Install-Program -AllowedArchitectures @("32-bit", "64-bit") -AllowedRegs @("clc","desktop","traveling","virtual-clc","virtual-desktop") -AllowedVersions @($OSVER.WIN7_SP0, $OSVER.WIN7_SP1) -ExitOnFail $True -Install install -PostInstall postinstall