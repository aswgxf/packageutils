#Error Codes
$global:ERROR_CODES = @{}
$global:ERROR_CODES.Add("EXIT_SUCCESS", 0)
$global:ERROR_CODES.Add("EXIT_FAILURE", 1)
$global:ERROR_CODES.Add("EXIT_INTERNAL_ERROR", 1359)
$global:ERROR_CODES.Add("EXIT_OS_VERSION_FAIL", 1690)
$global:ERROR_CODES.Add("EXIT_OS_ARCH_FAIL", 1691)
$global:ERROR_CODES.Add("EXIT_REGISTRATION_FAIL", 1692)
$global:ERROR_CODES.Add("EXIT_ALREADY_INSTALLED", 0)
$global:ERROR_CODES.Add("EXIT_PREREQ_FAIL", 1694)
$global:ERROR_CODES.Add("EXIT_COPY_SOURCEFILES_FAIL", 1695)
$global:ERROR_CODES.Add("EXIT_PREINSTALL_FAIL", 1696)
$global:ERROR_CODES.Add("EXIT_INSTALL_FAIL", 1697)
$global:ERROR_CODES.Add("EXIT_POSTINSTALL_FAIL", 1698)
$global:ERROR_CODES.Add("EXIT_VALIDATE_FAIL", 1699)

function Get-ScriptName {
    <#
        .Synopsis
        Gets the name of the script that called this function. (Does not return the module's name.)
    #>
    if ($MyInvocation.PSCommandPath) {
        (Get-Item $MyInvocation.PSCommandPath).BaseName
    } else {
        "CONSOLE"
    }
}

# Defaults
$OutputLog = Join-Path $ENV:TEMP "$(Get-ScriptName)-$(Get-Date -Format FileDate).txt"


function Get-WindowsVersion{
    $RegData = (Get-Item "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion")
    if ($RegData.GetValue("DisplayVersion")) {
        return $RegData.GetValue("DisplayVersion")
    } else {
        return $RegData.GetValue("ReleaseId")
    }
}

function Get-WindowsBuild {
    return (Get-Item "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion").GetValue("CurrentBuildNumber")
}

function Get-OSArchitecture{
    (Get-CimInstance Win32_OperatingSystem).OSArchitecture
}

function Get-Inst{
    Join-Path $Env:SystemRoot "system32" "umrinst"
}

function Get-Applogs{
    Join-Path (Get-Inst) "applogs"
}

function Get-FlagsDir{
    <#
        .Synopsis
        Returns the directory of the flags directory.

        .Description
        InstallMonkey can take configuration options via the command line
        and also by text files in the flags directory. This is primarily useful
        when a tester wishes to affect the script behavior of all InstallMonkey
        scripts run on a machine (e.g. ignoring the OS architecture for a
        test platform).

        .Example
        $flag_dir = Get-FlagsDir
        if(Test-Path "$flag_dir\IM_ignore_os_architecture"){ #Do Stuff }
    #>
    Join-Path (Get-Inst) "flags"
}





function Get-SourceFilesDir{
    <#
        .Synopsis
        Gets the source files directory
    #>
    Join-Path $ENV:SystemDrive "sourcefiles"
}




function New-Package{
    <#
        .Synopsis
        Creates a hash table with various properties about the package.

        .Parameter Version
        The package version with all periods "." replaced with underscores "_".

        .Parameter Name
        The package name containing no spaces

        .Parameter Revision
        The package revision in the form of YYYYMMDDTHHMM EX: 20160521T1443 (May 21st, 2016 at 2:43PM)

        .Example
        PS C:\>New-Package -name "WinSCP" -version "5_7_7" -Revision "20162105T1150"
        PS C:\>Write-Output $package.SourceFiles
        C:\sourcefiles\WinSCP.5_7_7
    #>
    param(
          [Parameter(Mandatory=$True)][string]$Version,
          [Parameter(Mandatory=$True)][string]$Name,
          [Parameter(Mandatory=$True)][string]$Revision
          )
    New-Variable -name 'package' -scope 'script'
    Remove-Variable 'package' -scope 'script'
    New-Variable -name 'package' -scope 'script'
    $script:package = @{}
    $script:package.Add("Version",$Version)
    $script:package.Add("Name",$Name)
    $script:package.Add("Revision",$Revision)
    $script:package.ID = ($script:package.Name + "." + $script:package.Version)
    $script:package.Add("SourceFiles",(Get-SourceFilesDir)+$Name+"."+$Version)
    return $script:package
}


function Install-Program{
    <#
        .Synopsis
        Starts the install script.

        .Description
        Installs program.

        .Parameter AppLog
        Name of output log. Default is packagename_output.txt

        .Parameter AllowedArchitectures
        An array of OS Architectures allowed to do the install (x86, x64)

        .Parameter AllowedRegs
        An array of host registrations allowed to do the install. (Desktop, CLC, Traveling, Virtual-Desktop, Virtual-ClC)

        .Parameter DataDir
        The directory in which the data to be copied resides. 'data' is the default value.

        .Parameter PreRequisite
        The function called for PreRequisites
        Occurs before source files are copied

        .Parameter PreInstall
        The function called for PreInstall
        Occurs right after source files are copied

        .Parameter Install
        The function called for Install

        .Parameter PostInstall
        The function called for PostInstall
        Occurs after install, but before registry key is created or install is logged via HTTP

        .Parameter Validate
        The function called for Validation
        Occurs last to verify everything is installed correctly

        .Parameter MSIName
        The name of the MSI to install.
        If not specified, and there is no Install parameter, defaults to 'installer.msi'

        .Parameter MSIArgs
        Additional arguments to provide to the MSI.

        .Parameter NeedReboot
        If $True, a message will be displayed saying that a reboot is necessary.

        .Parameter Scope
        The scope of the install (user-level or system-wide) 'user' or 'system' (default)

        .Parameter ExitOnFail
        If $True, exit if any of the install phases fails with a return code corresponding to the type of failure.

        .Parameter NoSourceFiles
        If $True, don't attempt to copy any files to sourcefiles.

        .Parameter NoAppLog
        If $True, don't attempt to log the install via HTTP

        .Parameter NoProductKey
        If $True, don't attempt to create the product key

        .Parameter NoInstallCheck
        If $True, don't attempt to check for existing package installation.

        .Parameter NoEventLog
        If $True, don't automatically create events in the Windows event log

        .Parameter NoOSArchCheck
        If $True, don't check the OS Architecture

        .Parameter NoRegCheck
        If $True, don't check the machine registration
    #>
    param(
        [string]$AppLog = ((Get-Applogs)+($Script:package.Name + "-" + $Script:package.Version)+"_Output.txt"),
        [bool]$ExitOnFail,
        [string[]]$AllowedArchitectures, 
        [string[]]$AllowedRegs, 
        [string]$DataDir= 'data', 
        [object]$PreRequisite, 
        [object]$PreInstall, 
        [string]$MSIName = 'installer.msi', 
        [string]$MSIArgs, 
        [object]$Install, 
        [object]$PostInstall, 
        [object]$Validate, 
        [bool]$NeedReboot, 
        [string]$InstallScope = 'system', 
        [bool]$NoSourceFiles, 
        [bool]$NoAppLog, 
        [bool]$NoProductKey, 
        [bool]$NoInstallCheck, 
        [bool]$NoEventLog, 
        [bool]$NoOSArchCheck, 
        [bool]$NoRegCheck)
    #to use passed functions: (&$install)
    $Script:OutputLog = $AppLog
    $StartTime = (Get-Date -Format u)
    Write-Logs ("Started logging to: $Script:OutputLog at " + $StartTime + "`n")
    Write-Logs ("Free Disk Space (" + (Get-Date -Format u) + ")")
    Write-Logs "==============================================================================="
    Write-Logs (Get-PSDrive @('C','D') | Select-Object Root, @{Name="Used (GB)";Expression={[math]::Round(($_.Used / 1GB),2)}},  @{Name="Free (GB)";Expression={[math]::Round(($_.Free / 1GB),2)}},  @{Name="Total (GB)";Expression={[math]::Round((($_.Used + $_.Free) / 1GB),2)}}, Free, Used, @{Name="Total";Expression={$_.Free + $_.Used}} | Format-Table | Out-String).Trim()
    Write-Logs "==============================================================================="
    Write-Logs "*******************************************************************************"
    Write-Logs ("Installing Application: " + $Script:package.ID + "  Revision: " + $Script:package.Revision)
    Write-Logs "*******************************************************************************"
    $OSArch = Get-OSArchitecture
    $Reg    = Get-MachineType
    $SuccessValues = @('',1)
    if(!($AllowedArchitectures -contains $OSArch) -and !$NoOSArchCheck){
        Write-Logs "This software cannot be installed on this OS Architecture. Current Architecture: $OSArch. Allowed Architectures: $AllowedArchitectures"
        Return $ERROR_CODES.EXIT_OS_ARCH_FAIL
    }
    if(!($AllowedRegs -contains $Reg) -and !$NoRegCheck){
        Write-Logs "This software cannot be installed on a $Reg computer. You must have one of these types: $AllowedRegs"
        Return $ERROR_CODES.EXIT_REGISTRATION_FAIL
    }
    if($PreRequisite){ 
        Write-Logs "`nPreRequisite Phase"
        Write-Logs "----------------"
        Write-Logs (Get-Date -Format u)
        Write-Logs " "
        $success = (Invoke-Expression $PreRequisite)
        if($success -notin $successValues){
            Write-Logs ("Install Failed! Return code: " + $ERROR_CODES.EXIT_PREREQ_FAIL)
            Write-Logs "PreReq returned: $Success"
            Return $ERROR_CODES.EXIT_PREREQ_FAIL
        }
    }
    if(!$NoSourceFiles){
        Write-Logs "`nCopySourceFiles Phase"
        Write-Logs "----------------"
        Write-Logs (Get-Date -Format u)
        Write-Logs " "
        $success = (Copy-SourceFiles -DataDir $data)
        if($success -notin $successValues){
            Write-Logs ("Install Failed! Return code: " + $ERROR_CODES.EXIT_COPY_SOURCEFILES_FAIL)
            Write-Logs "Copy Source Files returned: $Success"
            Return $ERROR_CODES.EXIT_COPY_SOURCEFILES_FAIL
        }
    }
    if($PreInstall){
        Write-Logs "`nPreInstall Phase"
        Write-Logs "----------------"
        Write-Logs (Get-Date -Format u)
        Write-Logs " "        
        $success = (Invoke-Expression $PreInstall)
        if($success -notin $successValues){
            Write-Logs ("Install Failed! Return code: " + $ERROR_CODES.EXIT_PREINSTALL_FAIL)
            Write-Logs "PreInstall returned: $Success"
            Return $ERROR_CODES.EXIT_PREINSTALL_FAIL
        }
    }
    if($Install){
        Write-Logs "`nInstall Phase"
        Write-Logs "----------------"
        Write-Logs (Get-Date -Format u)
        Write-Logs " "
        $success = (Invoke-Expression $Install)
        if($success -notin $successValues){
            Write-Logs ("Install Failed! Return code: " + $ERROR_CODES.EXIT_INSTALL_FAIL)
            Write-Logs "Install returned: $Success"
            Return $ERROR_CODES.EXIT_INSTALL_FAIL
        }
    }
    if($PostInstall){
        Write-Logs "`nPostInstall Phase"
        Write-Logs "----------------"
        Write-Logs (Get-Date -Format u)
        Write-Logs " "
        $success = (Invoke-Expression $PostInstall)
        if($success -notin $successValues){
            Write-Logs ("Install Failed! Return code: " + $ERROR_CODES.EXIT_POSTINSTALL_FAIL)
            Write-Logs "PostInstall returned: $Success"
            Return $ERROR_CODES.EXIT_POSTINSTALL_FAIL
        }
    }
    if($Validate){
        Write-Logs "`nValidate Phase"
        Write-Logs "----------------"
        Write-Logs (Get-Date -Format u)
        Write-Logs " "
        $success = (Invoke-Expression $Validate)
        if($success -notin $successValues){
            Write-Logs ("Install Failed! Return code: " + $ERROR_CODES.EXIT_VALIDATE_FAIL)
            Write-Logs "Validate returned: $Success"
            Return $ERROR_CODES.EXIT_VALIDATE_FAIL
        }
    }
    Write-Logs "`n`nInstall Successful!" 
    Write-Logs "----------------"
    Write-Logs ("Install finished at " + (Get-Date -Format u))
    Write-Logs ("This install took " + [String]([DateTime](Get-Date -Format u) - [DateTime]$StartTime) + ".")
    Write-Logs "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
}

function Get-MachineType{
    if($env:COMPUTERNAME -match '^rc\d\d')  { return 'clc' }
    if($env:COMPUTERNAME -match '^rcv\d\d') { return 'virtual-clc' }
    if($env:COMPUTERNAME -match '^rt\d\d')  { return 'traveling' }
    if($env:COMPUTERNAME -match '^rv\d\d')  { return 'virtual-desktop' }
    return 'desktop'
}

function Copy-SourceFiles{
    $data_dir = 'data'
    $dest_dir = $script:package.SourceFiles
    $source_dir = ($global:CWD)+"\$data_dir\*"
    Write-Logs ("Copying files from $source_dir to $dest_dir...`n" + (Get-Date -Format u))
    if(Test-Path($dest_dir)) { Remove-Item $dest_dir -recurse -force}
    New-Item -path $dest_dir -ItemType "directory" | Out-Null
    Copy-Item $source_dir $dest_dir -recurse -PassThru | Out-Null
    if(-not $?){
        Write-Logs "Copy Failed!"
        Return 0
    }
    Write-Logs "Finished Copying!"
    Return 1
}

function Write-Logs{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory, ValueFromPipeline)]
        [ValidateNotNullOrEmpty()]
        [string]
        $Message,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]
        $LogFile=$Script:OutputLog,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [ValidateSet('Verbose', 'Information', 'Warning', 'Error')]
        [string]
        $Severity = "Information",

        [Parameter()]
        [string]
        $EndString = "`n"
    )

    process {
        $LogEntry = @{}
        $LogEntry.Timestamp = (Get-Date -Format 'o')
        $LogEntry.Severity = $Severity.ToUpper()
        $LogEntry.Message = $message
        $LogEntry.CallingFunction = [string]$(Get-PSCallStack)[1].FunctionName
        $LogEntry.EndString = $EndString

        $Line = "$($LogEntry.Timestamp) | $($LogEntry.Severity) | $($LogEntry.CallingFunction) | $($LogEntry.Message)$($LogEntry.EndString)"
        Write-Host $Line
        Out-File -FilePath $LogFile -InputObject $Line -Append
    }
}

function Test-WriteLogs {
    Get-PSCallStack
    Write-Logs "This is another line of the log" -LogFile ".\log.txt" -Severity "Verbose"
}

function New-Shortcut{
    param(
        [Parameter(Mandatory=$True)][string]$target,
        [Parameter(Mandatory=$True)][string]$link,
        [string]$arguments,
        [string]$working,
        [string]$description,
        [string]$windowstyle = 'normal',
        [string]$icon="noicon",
        [string]$hotkey)
    Write-Logs "Creating shortcut to $target at $link"
    $windowstyles = @{}
    $windowstyles.Normal = 1
    $windowstyles.Maximized = 3
    $windowstyles.Minimized = 7
    $shell = New-Object -ComObject WScript.Shell
    $shortcut = $shell.CreateShortcut($link)
    $shortcut.TargetPath = $target
    $shortcut.WindowStyle = $windowstyles.$windowstyle
    if($shortcut.WorkingDirectory){$shortcut.WorkingDirectory = $working}
    if($shortcut.Arguments){$shortcut.Arguments = $arguments}
    if($shortcut.Description){$shortcut.Description = $description}
    if($shortcut.IconLocation -ne 'noicon'){$shortcut.IconLocation = $icon}
    if($shortcut.Hotkey){$shortcut.Hotkey = $hotkey}
    $shortcut.Save()
    Return 1
}

function Get-AllUsersStartMenu{
    return "$Env:ALLUSERSPROFILE\Microsoft\Windows\Start Menu"
}

<#
New-Package -Version "5_7_9" -Name "WinSCP" -Revision "20160527T1327"
Get-WindowsVersion
Get-OSArchitecture
Get-Inst
$ERROR_CODES
Get-FlagsDir
Install-Program
Get-SourceFilesDir
Copy-SourceFiles

#>

Export-ModuleMember -Function * -Alias * -Variable *